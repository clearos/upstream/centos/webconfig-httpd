# webconfig-httpd

Forked version of httpd in a chroot with ClearOS changes applied

* git clone git@gitlab.com:clearos/upstream/centos/webconfig-httpd.git
* cd webconfig-httpd
* git checkout clear7  # should already be on clear7
* git remote add upstream https://git.centos.org/rpms/httpd.git
* git pull upstream c7 # now fix merge conflicts

then
* git add .... # add the individual files to the commit
* git commit
or
* git commit -a # commits all files

Note: "make local" fails due to unpackaged debug files. Building through mock works.
